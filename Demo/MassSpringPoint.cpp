#include "MassSpringPoint.h"

MassSpringPoint::MassSpringPoint(XMVECTOR pos, XMVECTOR vel, double mass) :
MeshPoint(pos, mass){
	this->vel = vel;
	this->forces = XMVectorZero();
}