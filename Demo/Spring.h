#pragma once

#include "MassSpringPoint.h"

class Spring{
public:
	Spring(MassSpringPoint*, MassSpringPoint*, double stiffness, double length);
	MassSpringPoint* p1;
	MassSpringPoint* p2;
	double stiffness;
	double initLength;
};