#include "GameObject.h"

GameObject::GameObject(){
	this->pos = XMVectorZero();
	this->rotQuaternion = XMVectorZero();
	this->scale = XMVectorZero();
	this->mass = 0.;
//	this->constForces = XMVectorZero();
//	this->forceAccumulator = XMVectorZero();
	this->forceBuffer = XMVectorZero();
	this->sphereHullSquared = 0.;
}

GameObject::GameObject(XMVECTOR pos, XMVECTOR rotQuaternion, XMVECTOR scale,
	double mass){
	this->pos = pos;
	this->rotQuaternion = rotQuaternion;
	this->scale = scale;
	this->mass = mass;
//	this->constForces = XMVectorZero();
//	this->forceAccumulator = XMVectorZero();
	this->sphereHullSquared = 0.;
	this->forceBuffer = XMVectorZero();
}

XMVECTOR GameObject::getPosition(){
	return this->pos;
}
XMVECTOR GameObject::getRotation(){
	return this->rotQuaternion;
}
XMVECTOR GameObject::getScale(){
	return this->scale;
}
double GameObject::getSphereHullSquared(){
	return this->sphereHullSquared;
}