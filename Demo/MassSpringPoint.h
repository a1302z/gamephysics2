#pragma once

#include "Global.h"

class MassSpringPoint : public MeshPoint{
public:
	MassSpringPoint(XMVECTOR pos, XMVECTOR vel, double mass);
	XMVECTOR vel;
	XMVECTOR forces;
};
