#pragma once
#include "MeshPoint.h"

class FluidParticle : public MeshPoint{
public:
	FluidParticle(XMVECTOR pos);
	XMVECTOR forces;
	XMVECTOR vel;
};