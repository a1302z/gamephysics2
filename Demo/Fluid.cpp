#include "Fluid.h"

Fluid::Fluid(XMVECTOR posLowerLeftBorder, XMVECTOR sizeInDirections, int gridPointsPerAxis, double massPerParticle,
	double influenceDistance, double restDensity, double stiffness_k, bool useGrid) :
	GameObject(posLowerLeftBorder, XMVectorZero(), XMVectorZero(), massPerParticle)
{
	this->sizeInDirections = sizeInDirections;
	this->gridPointsPerAxis = gridPointsPerAxis;
	{
		std::vector<FluidParticle*> temp;
		std::vector<std::vector<FluidParticle*>> temp1(gridPointsPerAxis, temp);
		std::vector<std::vector<std::vector<FluidParticle*>>> temp2(gridPointsPerAxis, temp1);
		this->grid = std::vector<std::vector<std::vector<std::vector<FluidParticle*>>>>(gridPointsPerAxis, temp2);
	}
	//normally had to be calculated for each direction -> here it gets only calculated for x-Direction
	this->influenceDistance = influenceDistance;
	this->influenceDistanceInGridPoints = (int)(influenceDistance / (XMVectorGetX(this->sizeInDirections) / gridPointsPerAxis));
	this->rest_density = restDensity;
	this->stiffness_k = stiffness_k;
	this->useGrid = useGrid;
}

inline void Fluid::addPoint(FluidParticle point){
	if (useGrid){
		std::vector<FluidParticle*>* node = convertWorldSpaceToGridNode(point.pos);
		if (node && node != nullptr){
			particleStorage.push_back(point);
			node->push_back(&particleStorage[particleStorage.size() - 1]);
		}
	}
	else{
		point.pos -= this->pos;
		particleStorage.push_back(point);
	}
}

void Fluid::addPoints(std::vector<FluidParticle> points){
	for (int i = 0; i < points.size(); i++){
		addPoint(points[i]);
	}
}
//at the moment only works if this->pos = (0,0,0)
void Fluid::addRandomPoints(int number){
	srand(time(NULL));
	for (int i = 0; i < number; i++){
		XMVECTOR v = XMVectorSet(((double)rand() / double(RAND_MAX))*XMVectorGetX(this->sizeInDirections),
			((double)rand() / double(RAND_MAX))*XMVectorGetY(this->sizeInDirections),
			((double)rand() / double(RAND_MAX))*XMVectorGetZ(this->sizeInDirections), 0);
		this->addPoint(FluidParticle(v));
	}
}

inline std::vector<FluidParticle*>* Fluid::convertWorldSpaceToGridNode(XMVECTOR ws){
	ws -= this->pos;
	double x, y, z;
	x = XMVectorGetX(ws);
	y = XMVectorGetY(ws);
	z = XMVectorGetZ(ws);

	double xMax, yMax, zMax;
	xMax = XMVectorGetX(sizeInDirections);
	yMax = XMVectorGetY(sizeInDirections);
	zMax = XMVectorGetZ(sizeInDirections);

	if (x < 0 || x >= xMax
		|| y < 0 || y >= yMax
		|| z < 0 || z >= zMax){
		return nullptr;
	}

	int xInd, yInd, zInd;
	xInd = (int)(x / (xMax / (double)gridPointsPerAxis));
	yInd = (int)(y / (yMax / (double)gridPointsPerAxis));
	zInd = (int)(z / (zMax / (double)gridPointsPerAxis));

	return &grid[xInd][yInd][zInd];
}

void Fluid::update(double timestep){
	if (useGrid){
		//1. Compute Densities
		std::vector<std::vector<std::vector<double>>> density(gridPointsPerAxis,
			std::vector < std::vector<double>>(gridPointsPerAxis, std::vector<double>(gridPointsPerAxis, 0)));
		for (int i = 0; i < gridPointsPerAxis; i++){
			for (int j = 0; j < gridPointsPerAxis; j++){
				for (int k = 0; k < gridPointsPerAxis; k++){
					density[i][j][k] = 0;
					for (int l = 0; l < grid[i][j][k].size(); l++){
						FluidParticle* particle = this->grid[i][j][k][l];
						//Every Particle in influenceDistance
						for (int m = -influenceDistanceInGridPoints; m < influenceDistanceInGridPoints; m++){
							for (int n = -influenceDistanceInGridPoints; n < influenceDistanceInGridPoints; n++){
								for (int o = -influenceDistanceInGridPoints; o < influenceDistanceInGridPoints; o++){
									//Every Particle in Node
									int x, y, z;
									x = m + i;
									y = n + j;
									z = o + k;
									if (x < 0 || y < 0 || z < 0
										|| x > gridPointsPerAxis || y > gridPointsPerAxis > gridPointsPerAxis){
										continue;
									}
									for (int p = 0; p < grid[x][y][z].size(); p++){
										FluidParticle* other = this->grid[x][y][z][p];
										if (particle == other)
											continue;
										double q = this->calcQ(particle->pos, other->pos);
										density[i][j][k] += this->mass*this->calcSmoothingKernel(q);
									}
								}
							}
						}
					}

					if (density[i][j][k] < rest_density){
						density[i][j][k] = rest_density;
					}
				}
			}
		}
		//2. Compute Forces
		for (int i = 0; i < gridPointsPerAxis; i++){
			for (int j = 0; j < gridPointsPerAxis; j++){
				for (int k = 0; k < gridPointsPerAxis; k++){
					for (int l = 0; l < grid[i][j][k].size(); l++){
						FluidParticle* particle = this->grid[i][j][k][l];
						if (!particle || particle == nullptr)
							continue;
						//Always stops here because particle does not exist.. but it should be assured that it exists
						particle->forces = GLOBAL.gravity*this->mass*DOWN;
						//p_i = k*((rho_i/rho_0)^7-1)
						double p_i = this->stiffness_k*(pow((density[i][j][k] / this->rest_density), 7) - 1);
						for (int m = -influenceDistanceInGridPoints; m < influenceDistanceInGridPoints; m++){
							for (int n = -influenceDistanceInGridPoints; n < influenceDistanceInGridPoints; n++){
								for (int o = -influenceDistanceInGridPoints; o < influenceDistanceInGridPoints; o++){
									int x, y, z;
									x = m + i;
									y = n + j;
									z = o + k;
									if (x < 0 || y < 0 || z < 0
										|| x > gridPointsPerAxis || y > gridPointsPerAxis > gridPointsPerAxis){
										continue;
									}
									for (int p = 0; p < grid[x][y][z].size(); p++){
										FluidParticle* other = this->grid[x][y][z][p];
										if (other == particle)
											continue;
										double p_j = this->stiffness_k*(pow((density[x][y][z] / this->rest_density), 7) - 1);
										//f_press = - sum((p_i+p_j)/2 * (m/rho_i)*gradient
										particle->forces -= ((p_i + p_j)*0.5) * (this->mass / density[i][j][k]) *
											this->calcSmoothingGradient(this->calcQ(particle->pos, other->pos),
											particle->pos, other->pos);
									}
								}
							}
						}
						particle->pos += particle->vel * timestep;
						particle->vel += (particle->forces / mass) *timestep;
						double x, y, z;
						x = XMVectorGetX(particle->pos);
						y = XMVectorGetY(particle->pos);
						z = XMVectorGetZ(particle->pos);

						double xMax, yMax, zMax;
						xMax = XMVectorGetX(sizeInDirections);
						yMax = XMVectorGetY(sizeInDirections);
						zMax = XMVectorGetZ(sizeInDirections);

						if (x < 0 || x > xMax
							|| y < 0 || y > yMax
							|| z < 0 || z > zMax){
							particle->pos = XMVectorClamp(particle->pos, XMVectorZero(), this->sizeInDirections);
							particle->vel = -particle->vel;
						}
						this->grid[i][j][k].erase(this->grid[i][j][k].begin() + l);
						std::vector<FluidParticle*>* temp = convertWorldSpaceToGridNode(particle->pos);
						if (temp && temp != nullptr)temp->push_back(particle);
					}
				}
			}

		}
	}
	else{
		std::vector<double> densities(particleStorage.size(), 0.);
		for (int i = 0; i < particleStorage.size(); i++){
			particleStorage[i].forces = XMVectorZero();
			for (int j = i + 1; j < particleStorage.size(); j++){
				double q = this->calcQ(particleStorage[i].pos, particleStorage[j].pos);
				double density = this->mass*this->calcSmoothingKernel(q);
				densities[i] += density;
				densities[j] += density;
			}
			if (densities[i] < this->rest_density)
				densities[i] = this->rest_density;
		}
		for (int i = 0; i < particleStorage.size(); i++){
			double p_i = this->stiffness_k*(pow((densities[i] / this->rest_density), 7) - 1);
			for (int j = i + 1; j < particleStorage.size(); j++){
				double p_j = this->stiffness_k*(pow((densities[j] / this->rest_density), 7) - 1);
				//f_press = - sum((p_i+p_j)/2 * (m/rho_i)*gradient
				XMVECTOR f = ((p_i + p_j)*0.5) * (this->mass / densities[i]) *
					this->calcSmoothingGradient(this->calcQ(particleStorage[i].pos, particleStorage[j].pos),
					particleStorage[i].pos, particleStorage[j].pos);
				particleStorage[i].forces -= f;
				particleStorage[j].forces += f;
			}
		}
		for (int i = 0; i < particleStorage.size(); i++){
			particleStorage[i].pos += timestep*particleStorage[i].vel;
			particleStorage[i].vel += timestep*(particleStorage[i].forces + this->mass*GLOBAL.gravity*DOWN);

			double x, y, z;
			x = XMVectorGetX(particleStorage[i].pos);
			y = XMVectorGetY(particleStorage[i].pos);
			z = XMVectorGetZ(particleStorage[i].pos);

			double xMax, yMax, zMax;
			xMax = XMVectorGetX(sizeInDirections);
			yMax = XMVectorGetY(sizeInDirections);
			zMax = XMVectorGetZ(sizeInDirections);

			if (x < 0 || x > xMax
				|| y < 0 || y > yMax
				|| z < 0 || z > zMax){
				particleStorage[i].pos = XMVectorClamp(particleStorage[i].pos, XMVectorZero(), this->sizeInDirections);
				particleStorage[i].vel = -particleStorage[i].vel*GLOBAL.restitutionCoefficient;
			}
		}

	}
}




void Fluid::addForceAtPoint(XMVECTOR force, XMVECTOR point){

}

void Fluid::onCollide(CollisionInfo, GameObject* other){

}

void Fluid::getRenderInfo(std::vector<MeshPoint>* points, std::vector<line>* lines){
	for (int i = 0; i < this->particleStorage.size(); i++){
		points->push_back(MeshPoint(particleStorage[i].pos + this->pos, 4));
	}
}


CollisionInfo Fluid::collisionTest(std::vector<const MeshPoint*> otherPoints){
	CollisionInfo info;
	return info;
}

std::vector<const MeshPoint*> Fluid::getMeshPointsOfObject(){
	std::vector<const MeshPoint*> vector;
	return vector;
}


double Fluid::getMass(){
	return this->mass*this->particleStorage.size();
}

inline double Fluid::calcQ(XMVECTOR x1, XMVECTOR x2){
	return abs(XMVectorGetX(XMVector3Length(x1 - x2))) / (this->influenceDistance);
}

/*
Watch out if Q == 0 => return infinity
*/
inline double Fluid::calcSmoothingKernel(double Q){
	if (Q >= 2. || Q <= 0.) return 0;
	double factor = 0.5 * 3 / (pow(influenceDistance, 3)*3.14);
	if (Q >= 1.) return factor*(1. / 6.)*(2. - Q)*(2. - Q);
	return factor*((2. / 3.) - Q*Q + 1. / (2 * Q*Q*Q));
}
inline XMVECTOR Fluid::calcSmoothingGradient(double Q, XMVECTOR x1, XMVECTOR x2){
	if (Q >= 2.) return XMVectorZero();
	double length = XMVectorGetX(XMVector3Length(x1 - x2));
	if (length <= 0.) return XMVectorZero();
	XMVECTOR factor = ((x1 - x2) / abs(length)) * 9 * 0.25 / (3.14*pow(influenceDistance, 5));
	if (Q < 1) return factor*(Q - (4. / 3.)*Q*influenceDistance);
	return factor*(-pow(2 - Q, 2)*influenceDistance / 3.);
}