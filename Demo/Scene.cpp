#include "Scene.h"

Scene::Scene(ID3D11DeviceContext* context, BasicEffect* be, ID3D11InputLayout* il, CModelViewerCamera* cm){
	this->context = context;
	this->g_pEffectPositionNormal = be;
	this->g_pInputLayoutPositionNormal = il;
	this->g_camera = cm;
	g_pSphere = GeometricPrimitive::CreateGeoSphere(context, 2.0f, 2, false);
	//g_line = GeometricPrimitive::CreateCone(context, 0.01, 1, 2, false);
}

Scene::~Scene(){
	for (int i = 0; i < gameObjects.size(); i++){
		delete gameObjects[i];
		gameObjects[i] = nullptr;
	}
	gameObjects.clear();
	this->context = nullptr;
}

void Scene::addGameObject(GameObject *go){
	this->gameObjects.push_back(go);
}

void Scene::update(double timestep){
	//Collision Test
	for (int i = 0; i < gameObjects.size(); i++){
		for (int j = i + 1; j < gameObjects.size(); j++){
			//Broad-Phase
			//Sphere Bounding Boxes
			if (XMVectorGetX(XMVector3LengthSq(gameObjects[i]->getPosition() - gameObjects[j]->getPosition()))
				< gameObjects[i]->getSphereHullSquared() + gameObjects[j]->getSphereHullSquared()){
				//Narrow Phase
				CollisionInfo i1 = gameObjects[i]->collisionTest(gameObjects[j]->getMeshPointsOfObject());
				CollisionInfo i2 = gameObjects[j]->collisionTest(gameObjects[i]->getMeshPointsOfObject());
				if (i1.valid)
				{
					gameObjects[j]->onCollide(i1, gameObjects[i]);
				}if (i2.valid){
					gameObjects[i]->onCollide(i2, gameObjects[j]);
				}
			}
		}
	}
	for (int i = 0; i < gameObjects.size(); i++){
		gameObjects[i]->update(timestep);
	}
}

void Scene::render(){
	this->points.clear();
	this->lines.clear();
	for (int i = 0; i < gameObjects.size(); i++){
		gameObjects[i]->getRenderInfo(&points, &lines);
	}
	//points.push_back(XMVectorZero());
	g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormal->SetSpecularPower(100);
	for (int i = 0; i < points.size(); i++)
	{
		// Setup position/normal effect (per object variables)
		if (abs(points[i].attribute - 1) < 0.1){
			//red
			g_pEffectPositionNormal->SetDiffuseColor(0.6f * XMColorHSVToRGB(XMVectorSet(1, 1, 1, 0)));
		}
		else if (abs(points[i].attribute - 2) < 0.1){
			//blue
			g_pEffectPositionNormal->SetDiffuseColor(XMVectorSet(0, 0, 1, 0));
		}
		else if (abs(points[i].attribute - 3) < 0.1){
			//green
			g_pEffectPositionNormal->SetDiffuseColor(XMVectorSet(0, 1, 0, 0));
		}
		double size = 0.01;
		XMMATRIX scale = XMMatrixScaling(size, size, size);
		XMMATRIX trans = XMMatrixTranslationFromVector(points[i].pos);
		g_pEffectPositionNormal->SetWorld(scale * trans * g_camera->GetWorldMatrix());

		// Draw
		// NOTE: The following generates one draw call per object, so performance will be bad for n>>1000 or so
		g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
	}

}

void Scene::resetScene(){
	this->points.clear();
	this->lines.clear();
	for (int i = 0; i < gameObjects.size(); i++){
		delete gameObjects[i];
		gameObjects[i] = nullptr;
	}
	this->gameObjects.clear();
}

void Scene::printPositions(){
	for (int i = 0; i < gameObjects.size(); i++){
		XMVECTOR pos = gameObjects[i]->getPosition();
		double x, y, z;
		x = XMVectorGetX(pos);
		y = XMVectorGetY(pos);
		z = XMVectorGetZ(pos);
		printf("%i. Game Object is at (%f, %f, %f)\n", i, x, y, z);
	}
}