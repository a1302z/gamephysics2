#pragma once

#include <DirectXMath.h>
#include <vector>

using namespace DirectX;

class MeshPoint{
public:
	MeshPoint(XMVECTOR, double);
	XMVECTOR pos;
	double attribute;
};