#pragma once
#include "GameObject.h"


/*
this-> pos is position of center in world space
points.pos is position of massnode in object space relative to center
*/
class MassSpring : public GameObject{
public:

	MassSpring(std::vector<MassSpringPoint>, std::vector<Spring>);
	void update(double timestep);
	void addForceAtPoint(XMVECTOR force, XMVECTOR point);
	void onCollide(CollisionInfo info, GameObject* other);
	void getRenderInfo(std::vector<MeshPoint>* points, std::vector<line>* lines);

	void addMassPoint(MassSpringPoint newPoint);
	void addSpring(Spring spring);
	void addMassPoints(std::vector<MassSpringPoint> newPoint);
	void addSprings(std::vector<Spring> springs);

	CollisionInfo collisionTest(std::vector<const MeshPoint*> otherPoints);
	std::vector<const MeshPoint*> getMeshPointsOfObject();

	std::vector<MassSpringPoint>* getPoints();

private:
	//Points relative to the GameObject
	std::vector<MassSpringPoint> points;
	std::vector<Spring> springs;

	void recalculateMidpoint();
	void recalculateHull();
	void recalculateMassGravity();
};