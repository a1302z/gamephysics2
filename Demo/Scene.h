#pragma once

#include "GameObject.h"
#include "MassSpring.h"
#include "RigidBody.h"
#include "Fluid.h"
#include <stdio.h>

class Scene{
private:

	std::vector<MeshPoint> points;
	std::vector<line> lines;

	std::vector<GameObject*> gameObjects;

	ID3D11DeviceContext* context;
	BasicEffect* g_pEffectPositionNormal;
	ID3D11InputLayout* g_pInputLayoutPositionNormal;
	CModelViewerCamera* g_camera;
	std::unique_ptr<GeometricPrimitive> g_pSphere;
	std::unique_ptr<GeometricPrimitive> g_line;

public:
	Scene(ID3D11DeviceContext*, BasicEffect*, ID3D11InputLayout*, CModelViewerCamera*);
	~Scene();

	void addGameObject(GameObject*);
	void resetScene();
	void update(double timestep);
	void render();

	void printPositions();
};