#include "Spring.h"

Spring::Spring(MassSpringPoint* p1, MassSpringPoint* p2, double stiffness, double length){
	this->p1 = p1;
	this->p2 = p2;
	this->stiffness = stiffness;
	this->initLength = length;
}