#include "FluidParticle.h"

FluidParticle::FluidParticle(XMVECTOR pos) : MeshPoint(pos, 0.){
	this->forces = XMVectorZero();
	this->vel = XMVectorZero();
}