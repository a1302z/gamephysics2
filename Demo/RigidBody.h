#pragma once

#include "GameObject.h"

/*
Pos: Position of center of mass in world space
Scale is mostly ignored -> if should be used has to be implemented

Nodes have two positions -> Pos is position relative to center of mass in rest state
						-> rel Pos is current position relative to center of mass (with rotation)
*/
class RigidBody : public GameObject{
public:
	RigidBody(XMVECTOR pos, XMVECTOR rot, XMVECTOR scale, std::vector<RigidBodyPoint> points);
	void addRigidBodyPoints(std::vector<RigidBodyPoint>);
	void addRigidBodyPoint(RigidBodyPoint);

	void update(double timestep);
	void addForceAtPoint(XMVECTOR force, XMVECTOR point);
	void onCollide(CollisionInfo, GameObject* other);
	void getRenderInfo(std::vector<MeshPoint>* points, std::vector<line>* lines);

	CollisionInfo collisionTest(std::vector<const MeshPoint*> otherPoints);
	std::vector<const MeshPoint*> getMeshPointsOfObject();
private:
	//points always in object space!
	std::vector<RigidBodyPoint> points;

	XMVECTOR linearVelocity_V;

	XMMATRIX inertiaTensorInverse_0;
	XMMATRIX inertiaTensorInverse;
	XMVECTOR angularVelocity_w;
	XMVECTOR angularMomentum_L;

	void precompute();

	inline void updateInertiaTensorInverse();
	inline void updateAngularValues();
	inline void onWallImpulse(XMVECTOR normal, int pointIndex);
};