#include "RigidBody.h"


//Note: scale is not used everywhere it would be required!!
RigidBody::RigidBody(XMVECTOR pos, XMVECTOR rot, XMVECTOR scale, std::vector<RigidBodyPoint> points)
: GameObject(pos, rot, scale, 0)
{
	this->points = points;
	linearVelocity_V = XMVectorZero();
	precompute();
}

void RigidBody::precompute(){
	//Calculate total mass + center of mass
	XMVECTOR pos = XMVectorZero();
	this->linearVelocity_V = XMVectorZero();
	for (int i = 0; i < points.size(); i++){
		this->mass += points[i].attribute;
		pos += points[i].relPos * points[i].attribute;
		this->linearVelocity_V += points[i].vel*points[i].attribute;
	}
	this->pos += pos / this->mass;
	this->linearVelocity_V /= this->mass;
	//translate points into ObjectSpace
	for (int i = 0; i < points.size(); i++){
		points[i].pos -= this->pos;
		points[i].relPos = XMVector3Rotate(points[i].pos, this->rotQuaternion);
	}
	//Compute inertia Tensor inverse without rotation
	inertiaTensorInverse_0 = XMMatrixIdentity() - XMMatrixIdentity();
	double matrixSubstitute[4][4];
	for (int i = 0; i < points.size(); i++){
		//C_{j,k} = sum_{n} m_n * x_{n,j} * x_{n,k}
		//I = Id * trace(C) - C
		double mass = points[i].attribute;
		for (int j = 0; j < 4; j++){
			for (int k = 0; k < 4; k++){
				matrixSubstitute[j][k] = 0.;
			}
		}
		for (int j = 0; j < 4; j++){
			for (int k = 0; k < 4; k++){
				matrixSubstitute[j][k] -= mass* XMVectorGetByIndex(points[i].relPos, j)
					* XMVectorGetByIndex(points[i].relPos, k);
			}
		}
	}
	double traceC = matrixSubstitute[0][0] + matrixSubstitute[1][1] + matrixSubstitute[2][2];
	inertiaTensorInverse_0 = XMMatrixSet(matrixSubstitute[0][0], matrixSubstitute[0][1], matrixSubstitute[0][2], matrixSubstitute[0][3],
		matrixSubstitute[1][0], matrixSubstitute[1][1], matrixSubstitute[1][2], matrixSubstitute[1][3],
		matrixSubstitute[2][0], matrixSubstitute[2][1], matrixSubstitute[2][2], matrixSubstitute[2][3],
		matrixSubstitute[3][0], matrixSubstitute[3][1], matrixSubstitute[3][2], matrixSubstitute[3][3]);
	inertiaTensorInverse_0 += XMMatrixIdentity()*traceC;
	inertiaTensorInverse_0 = XMMatrixInverse(nullptr, inertiaTensorInverse_0);

	updateInertiaTensorInverse();
	updateAngularValues();
}

inline void RigidBody::updateInertiaTensorInverse(){
	XMMATRIX rotMatrix = XMMatrixRotationQuaternion(this->rotQuaternion);
	inertiaTensorInverse = XMMatrixMultiply(XMMatrixMultiply(rotMatrix, inertiaTensorInverse_0),
		XMMatrixInverse(nullptr, rotMatrix));
}


void RigidBody::addRigidBodyPoints(std::vector<RigidBodyPoint> newPoints){
	for (int i = 0; i < newPoints.size(); i++){
		points.push_back(newPoints[i]);
	}
	precompute();
}

void RigidBody::addRigidBodyPoint(RigidBodyPoint p){
	points.push_back(p);
	precompute();
}


void RigidBody::update(double timestep){
	if (timestep <= 0) return;
	XMVECTOR externalForces = XMVectorZero();
	XMVECTOR torque = XMVectorZero();
	for (int i = 0; i < points.size(); i++){
		externalForces += points[i].force;
		torque += XMVector3Cross(points[i].relPos, points[i].force + GLOBAL.gravity*points[i].attribute*DOWN);
		points[i].force = XMVectorZero();
	}
	//Damping Forces
	//externalForces -= GLOBAL.frictionCoefficient*this->linearVelocity_V;
	//torque -= GLOBAL.frictionCoefficient*this->angularVelocity_w;

	//Euler step
	this->pos += linearVelocity_V*timestep;
	this->linearVelocity_V += (externalForces + this->mass*GLOBAL.gravity*DOWN)*timestep / this->mass;

	//Angular updates
	/*this->rotQuaternion += timestep / 2. *  XMVectorSet(XMVectorGetX(this->angularVelocity_w),
		XMVectorGetY(this->angularVelocity_w), XMVectorGetZ(this->angularVelocity_w), 0) * this->rotQuaternion;*/

	updateInertiaTensorInverse();
	angularMomentum_L += timestep * torque;

	angularVelocity_w = (XMVector3Transform(angularMomentum_L, inertiaTensorInverse));
	//m_rotation += deltaTime / 2.0f * XMQuaternionMultiply(m_angularV, m_rotation);
	this->rotQuaternion += timestep / 2. *XMQuaternionMultiply(this->angularVelocity_w,
		this->rotQuaternion);
	this->rotQuaternion = XMQuaternionNormalize(this->rotQuaternion);
	for (int i = 0; i < points.size(); i++){
		//XMMATRIX rotMatrix = XMMatrixRotationQuaternion(this->rotQuaternion);
		points[i].relPos = XMVector3Rotate(points[i].pos, this->rotQuaternion);
		points[i].vel = XMVector3Cross(angularVelocity_w, points[i].relPos);

		//Clamping position, same as in MassSpring
		double rightBorder = XMVectorGetX(GLOBAL.borders);
		double leftBorder = -1 * rightBorder;
		double xPos = XMVectorGetX(points[i].relPos + this->pos);
		double xVel = XMVectorGetX(points[i].vel + this->linearVelocity_V);

		double upperBorder = XMVectorGetY(GLOBAL.borders);
		double lowerBorder = -1 * upperBorder;
		double yPos = XMVectorGetY(points[i].relPos + this->pos);
		double yVel = XMVectorGetY(points[i].vel + this->linearVelocity_V);

		double backBorder = XMVectorGetZ(GLOBAL.borders);
		double frontBorder = -1 * backBorder;
		double zPos = XMVectorGetZ(points[i].relPos + this->pos);
		double zVel = XMVectorGetZ(points[i].vel + this->linearVelocity_V);

		if (xPos < leftBorder && xVel < 0.){
			onWallImpulse(RIGHT, i);
		}
		if (xPos > rightBorder && xVel > 0.){
			onWallImpulse(RIGHT, i);
		}
		if (yPos < lowerBorder && yVel < 0.){
			onWallImpulse(UP, i);
		}
		if (yPos > upperBorder && yVel > 0.){
			onWallImpulse(-UP, i);
		}
		if (zPos < frontBorder && zVel < 0.){
			onWallImpulse(BACK, i);
		}
		if (zPos > backBorder && zVel > 0.){
			onWallImpulse(-BACK, i);
		}

	}
}


void RigidBody::addForceAtPoint(XMVECTOR force, XMVECTOR point){

}

void RigidBody::getRenderInfo(std::vector<MeshPoint>* points, std::vector<line>* lines){
	for (int i = 0; i < this->points.size(); i++){
		points->push_back(MeshPoint(this->points[i].relPos + this->pos, 3));
	}
	points->push_back(MeshPoint(this->pos, 1));
}

CollisionInfo RigidBody::collisionTest(std::vector<const MeshPoint*> otherPoints){
	CollisionInfo info;
	std::vector<int> possiblePointsIndices;
	for (int i = 0; i < otherPoints.size(); i++){
		if (XMVectorGetX(XMVector3LengthSq(otherPoints[i]->pos - this->pos)) < this->sphereHullSquared){
			possiblePointsIndices.push_back(i);
		}
	}
	//..

	return info;
}

std::vector<const MeshPoint*> RigidBody::getMeshPointsOfObject(){
	std::vector<const MeshPoint*> p;
	for (int i = 0; i < this->points.size(); i++){
		p.push_back(&this->points[i]);
	}
	return p;
}

inline void RigidBody::updateAngularValues(){
	this->angularMomentum_L = XMVectorZero();
	for (int i = 0; i < points.size(); i++){
		angularMomentum_L += XMVector3Cross(points[i].relPos, points[i].vel*points[i].attribute);
	}
	angularVelocity_w = XMVector3Transform(angularMomentum_L, inertiaTensorInverse);
}

void RigidBody::onCollide(CollisionInfo info, GameObject* other){

}

//Impulse with fixed body (e.g. floors)
inline void RigidBody::onWallImpulse(XMVECTOR normal, int pointIndex){
	/*
	J = (-(1+c)v_{rel} * n)/
	(1/M + [(I^{-1}(pos x n))x pos)] * n)
	*/
	XMVECTOR v = this->linearVelocity_V + points[pointIndex].vel;
	double impulseJ = abs((-(1 + GLOBAL.restitutionCoefficient)*
		XMVectorGetX(XMVector3Dot(this->linearVelocity_V + points[pointIndex].vel,
		normal))) /
		((1 / this->mass) + XMVectorGetX(XMVector3Dot(XMVector3Cross(
		XMVector3Transform(XMVector3Cross(points[pointIndex].relPos, normal)
		, inertiaTensorInverse), points[pointIndex].relPos), normal))));

	printf("Impulse: %f\n", impulseJ);
	this->linearVelocity_V += impulseJ*normal / this->mass;
	//Somehow the linear velocity is reasonable but the angular momentum explodes...
	this->angularMomentum_L += XMVector3Cross(points[pointIndex].relPos, impulseJ * normal);

}