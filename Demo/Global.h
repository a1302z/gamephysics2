#pragma once

//#include <DirectXMath.h>
//#include <vector>
#include <d3dx11effect.h>

#include <sstream>
#include <iomanip>
#include <random>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

#include <stdlib.h> 
#include <time.h> 

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;
using std::cout;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"

#include "MeshPoint.h"


//using namespace DirectX;

#define GLOBAL Global::getInstance()
#define DOWN XMVectorSet(0, -1, 0, 0)
#define UP XMVectorSet(0, 1, 0, 0)
#define RIGHT XMVectorSet(1, 0, 0, 0)
#define BACK XMVectorSet(0, 0, 1,0)

#define QUAT_ZERO XMQuaternionRotationMatrix(XMMatrixIdentity())

struct CollisionInfo{
	bool valid = false;
	XMVECTOR normal = XMVectorZero();
	XMVECTOR posInWorldSpace = XMVectorZero();
	double impulse = 0;
	MeshPoint* evtlPoint = nullptr;
};



class Global{
public:
	static Global& getInstance(){
		static Global instance;
		return instance;
	}
	double gravity;
	XMVECTOR borders;

	double restitutionCoefficient;
	double frictionCoefficient;
	double maxTimestep;
private:
	Global() :
		gravity(5.),
		borders(XMVectorSet(0.5, 0.5, 0.5, 0)),
		restitutionCoefficient(0.3),
		frictionCoefficient(0.8),
		maxTimestep(0.0025)
	{};
};