#pragma once

#include <DirectXMath.h>
#include <vector>

using namespace DirectX;


class line{
public:
	line(XMVECTOR sp, XMVECTOR direction);
	XMVECTOR startPoint;
	XMVECTOR dir;
};