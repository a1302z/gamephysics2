#pragma once

#include "Global.h"
#include "Line.h"
#include "MassSpringPoint.h"
#include "RigidBodyPoint.h"
#include "FluidParticle.h"
#include "Spring.h"

class GameObject{
protected:
	GameObject();
	GameObject(XMVECTOR pos, XMVECTOR rotQuaternion, XMVECTOR scale,
		double mass);

	XMVECTOR pos;
	XMVECTOR rotQuaternion;
	XMVECTOR scale;

	double mass;

	//XMVECTOR constForces;
	//XMVECTOR forceAccumulator;

	//Maybe useful for user interaction
	XMVECTOR forceBuffer;

	double sphereHullSquared;
public:

	virtual void update(double timestep){}
	virtual void addForceAtPoint(XMVECTOR force, XMVECTOR point){}
	virtual void onCollide(CollisionInfo info, GameObject* other){}
	virtual void getRenderInfo(std::vector<MeshPoint>* points, std::vector<line>* lines){}

	virtual CollisionInfo collisionTest(std::vector<const MeshPoint*> otherPoints){ CollisionInfo info; return info; }
	virtual std::vector<const MeshPoint*> getMeshPointsOfObject(){ return std::vector<const MeshPoint*>(); }

	XMVECTOR getPosition();
	XMVECTOR getRotation();
	XMVECTOR getScale();

	virtual double getMass(){ return this->mass; }

	double getSphereHullSquared();
};