#pragma once

#include "Global.h"

class RigidBodyPoint : public MeshPoint{
public:
	RigidBodyPoint(XMVECTOR relpos, XMVECTOR vel, double mass);
	XMVECTOR relPos;
	XMVECTOR vel;
	XMVECTOR force;
};