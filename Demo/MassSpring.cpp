#include "MassSpring.h"

MassSpring::MassSpring(std::vector<MassSpringPoint> mps, std::vector<Spring> s) : GameObject(){
	for (int i = 0; i < mps.size(); i++){
		this->points.push_back(mps[i]);
	}
	for (int i = 0; i < s.size(); i++){
		this->springs.push_back(s[i]);
	}

	this->recalculateMidpoint();
	this->recalculateHull();
	this->recalculateMassGravity();
}

/*
Asserts that the Position of the GameObject ist in the middle of all points of the object
-> Important because of sphere hulls for collision detection
*/
void MassSpring::recalculateMidpoint(){
	XMVECTOR oldmid = this->pos;
	XMVECTOR mid = XMVectorZero();
	for (int i = 0; i < points.size(); i++){
		mid += points[i].pos + oldmid;//absolute world position
	}
	mid /= points.size();
	this->pos = mid;
	for (int i = 0; i < points.size(); i++){
		//Points in Object space
		points[i].pos += oldmid;
		points[i].pos -= mid;
	}
}
/*
Asserts that all points are enclosed in the sphere hull
*/
void MassSpring::recalculateHull(){
	for (int i = 0; i < points.size(); i++){
		double pointDistanceSq = XMVectorGetX(XMVector3LengthSq(points[i].pos));
		if (pointDistanceSq > this->sphereHullSquared){
			this->sphereHullSquared = pointDistanceSq;
		}
	}
}

void MassSpring::recalculateMassGravity(){
	this->mass = 0.;
	//this->constForces = XMVectorZero();
	for (int i = 0; i < points.size(); i++){
		this->mass += points[i].attribute;
	}
	//this->constForces = this->mass*GLOBAL.gravity*XMVectorSet(0, -1, 0, 0);
}

void MassSpring::addMassPoint(MassSpringPoint mp){
	this->points.push_back(mp);
	this->recalculateMidpoint();
	this->recalculateHull();
	this->recalculateMassGravity();
}

void MassSpring::addMassPoints(std::vector<MassSpringPoint> mps){
	for (int i = 0; i < mps.size(); i++){
		this->points.push_back(mps[i]);
	}
	this->recalculateMidpoint();
	this->recalculateHull();
	this->recalculateMassGravity();
}
void MassSpring::addSpring(Spring s){
	this->springs.push_back(s);
}
void MassSpring::addSprings(std::vector<Spring> springs){
	for (int i = 0; i < springs.size(); i++){
		this->springs.push_back(springs[i]);
	}
}
void MassSpring::addForceAtPoint(XMVECTOR force, XMVECTOR point){
	MassSpringPoint mp = points[0];
	XMVECTOR shortestDistance = (this->pos + points[0].pos) - point;
	for (int i = 1; i < points.size(); i++){
		if (XMVector3Less((this->pos + points[i].pos) - point, shortestDistance)){
			mp = points[i];
			shortestDistance = (this->pos + points[i].pos) - point;
		}
	}
	mp.forces += force;
}

CollisionInfo MassSpring::collisionTest(std::vector<const MeshPoint*> otherPoints){
	CollisionInfo info;
	return info;
}

void MassSpring::update(double timestep){
	if (timestep <= 0) return;
	for (int i = 0; i < springs.size(); i++){
		//F_ij = -k(l-L)(x_i-x_j)/l
		XMVECTOR force = (-springs[i].stiffness*
			(springs[i].initLength - XMVectorGetX(XMVector3Length(springs[i].p1->pos - springs[i].p2->pos)))*
			(springs[i].p1->pos - springs[i].p2->pos) / springs[i].initLength);
		springs[i].p1->forces -= force;
		springs[i].p2->forces += force;
	}
	double upperBorder = XMVectorGetY(GLOBAL.borders);
	double lowerBorder = -1 * upperBorder;
	for (int i = 0; i < points.size(); i++){
		points[i].pos += timestep * points[i].vel;

		points[i].forces -= points[i].vel*GLOBAL.frictionCoefficient;
		points[i].vel += timestep * (points[i].forces + points[i].attribute*GLOBAL.gravity * DOWN)/points[i].attribute;


		double yPos = XMVectorGetY(points[i].pos + this->pos);
		double yVel = XMVectorGetY(points[i].vel);

		bool cond = yPos < lowerBorder && yVel < 0.;

		if (cond){
			points[i].vel += 2 * UP*abs(yVel);
			if (abs(yVel) < 0.15){
				points[i].vel *= XMVectorSet(1, 0.001, 1, 1);
			}
		}

		points[i].forces = XMVectorZero();
	}
	this->recalculateMidpoint();
	this->recalculateHull();
}

void MassSpring::getRenderInfo(std::vector<MeshPoint>* points, std::vector<line>* lines){
	for (int i = 0; i < this->points.size(); i++){
		points->push_back(MeshPoint(this->points[i].pos + this->pos, 1));
	}
	points->push_back(MeshPoint(this->pos, 2));
	for (int i = 0; i < this->springs.size(); i++){
		lines->push_back(line(this->springs[i].p1->pos + this->pos, (this->springs[i].p2->pos + this->pos - this->springs[i].p1->pos + this->pos)));
	}
}

std::vector<MassSpringPoint>* MassSpring::getPoints(){
	return &points;
}

std::vector<const MeshPoint*> MassSpring::getMeshPointsOfObject(){
	std::vector<const MeshPoint*> p;
	for (int i = 0; i < points.size(); i++){
		p.push_back(&points[i]);
	}
	return p;
}


void MassSpring::onCollide(CollisionInfo info, GameObject* other){

}