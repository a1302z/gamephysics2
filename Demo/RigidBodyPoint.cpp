#include "RigidBodyPoint.h"


RigidBodyPoint::RigidBodyPoint(XMVECTOR pos, XMVECTOR vel, double mass)
: MeshPoint(pos, mass){
	this->relPos = pos;
	this->vel = vel;
	this->force = XMVectorZero();
}