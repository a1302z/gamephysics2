#pragma once

#include "GameObject.h"
/*
Pos: World Position of lower left Point of Grid
sizeInDirections: size of grid in directions
this->mass: Here always the mass of only ONE(!!) particle

rot & scale are ignored..
*/
class Fluid : public GameObject{
public:
	Fluid(XMVECTOR pos, XMVECTOR sizeInDirections, int gridPointsPerAxis, double massPerParticle,
		double influenceDistance, double restDensity, double stiffness_k, bool useGrid);
	inline void addPoint(FluidParticle newNode);
	void addPoints(std::vector<FluidParticle> newNodes);
	void addRandomPoints(int number);

	void update(double timestep);
	void addForceAtPoint(XMVECTOR force, XMVECTOR point);
	void onCollide(CollisionInfo, GameObject* other);
	void getRenderInfo(std::vector<MeshPoint>* points, std::vector<line>* lines);

	CollisionInfo collisionTest(std::vector<const MeshPoint*> otherPoints);
	std::vector<const MeshPoint*> getMeshPointsOfObject();

	double getMass();

private:
	bool useGrid;
	XMVECTOR sizeInDirections;
	int gridPointsPerAxis;
	double influenceDistance;
	int influenceDistanceInGridPoints;
	double rest_density;
	double stiffness_k;
	std::vector<FluidParticle> particleStorage;

	std::vector<std::vector<std::vector<std::vector<FluidParticle*>>>> grid;

	inline std::vector<FluidParticle*>* convertWorldSpaceToGridNode(XMVECTOR ws);

	inline double calcQ(XMVECTOR x1, XMVECTOR x2);
	inline double calcSmoothingKernel(double Q);
	inline XMVECTOR calcSmoothingGradient(double Q, XMVECTOR x1, XMVECTOR x2);
};